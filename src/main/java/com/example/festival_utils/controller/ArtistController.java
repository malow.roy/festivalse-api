package com.example.festival_utils.controller;

import com.example.festival_utils.model.Artist;
import com.example.festival_utils.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/artist")
public class ArtistController {

    private final ArtistRepository ar;

    @Autowired
    public ArtistController(ArtistRepository ar) {
        this.ar = ar;
    }

    @GetMapping("")
    public List<Artist> getArtists()
    {
        return ar.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Artist> getArtistById(@PathVariable("id") long id)
    {
        Optional<Artist> artistData = ar.findById(id);

        return artistData.map(artist -> new ResponseEntity<>(artist, HttpStatus.NO_CONTENT)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("")
    public ResponseEntity<Artist> createArtist(@RequestBody Artist a)
    {
        try
        {
            Artist _artist = ar.save(new Artist(a.getName(), a.getStartHour(), a.getEndHour()));
            return new ResponseEntity<>(_artist, HttpStatus.CREATED);
        }
        catch(Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Artist> updateArtist(@PathVariable("id") long id, @RequestBody Artist a)
    {
        Optional<Artist> artistData = ar.findById(id);

        if(artistData.isPresent())
        {
            Artist _artist = artistData.get();
            _artist.setName(a.getName());
            _artist.setStartHour(a.getStartHour());
            _artist.setEndHour(a.getEndHour());
            return new ResponseEntity<>(ar.save(_artist), HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteArtist(@PathVariable("id") long id)
    {
        try
        {
            ar.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
