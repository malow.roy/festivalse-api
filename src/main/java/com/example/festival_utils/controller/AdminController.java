package com.example.festival_utils.controller;

import com.example.festival_utils.entity.admin.AdminAuthImpl;
import com.example.festival_utils.repository.PersonRepository;
import com.example.festival_utils.security.jwt.JwtUtils;
import com.example.festival_utils.entity.admin.Admin;
import com.example.festival_utils.payload.response.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.password.PasswordEncoder;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/admin")
public class AdminController {

    final PersonRepository adminRepository;

    final PasswordEncoder encoder;

    final AuthenticationManager authenticationManager;

    final JwtUtils jwtUtils;

    @Autowired
    public AdminController(
            PersonRepository adminRepository,
            PasswordEncoder encoder,
            @Qualifier("admin_manager") AuthenticationManager authenticationManager,
            JwtUtils jwtUtils
    ) {
        this.adminRepository = adminRepository;
        this.authenticationManager = authenticationManager;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(String email, String password) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        AdminAuthImpl adminImpl = (AdminAuthImpl) authentication.getPrincipal();

        String jwt = jwtUtils.generateJwtToken(adminImpl.getUsername());

        return ResponseEntity.ok(new JwtResponse(jwt));
    }



    @PostMapping("/register")
    public ResponseEntity<?> register(String email, String password) {
        try {
            Admin newAdmin = new Admin(
                    email,
                    encoder.encode(password)
            );
            adminRepository.save(newAdmin);
            return new ResponseEntity<>(
                    "C'est ok",
                    HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(
                    "C'est pas ok",
                    HttpStatus.BAD_REQUEST);
        }
    }

}
