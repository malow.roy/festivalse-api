package com.example.festival_utils.controller;

import com.example.festival_utils.model.Artist;
import com.example.festival_utils.model.Festival;
import com.example.festival_utils.repository.FestivalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/festival")
public class FestivalController {

    private final FestivalRepository fr;

    @Autowired
    public FestivalController(FestivalRepository fr) {
        this.fr = fr;
    }

    /**
     *
     * @return all festivals
     */
    @GetMapping("")
    public List<Festival> getFestivals()
    {
        return fr.findAll();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Festival> getFestivalById(@PathVariable("id") long id)
    {
        Optional<Festival> festivalData = fr.findById(id);

        return festivalData.map(festival -> new ResponseEntity<>(festival, HttpStatus.NO_CONTENT)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("")
    public ResponseEntity<Festival> createFestival(@RequestBody Festival a)
    {
        try
        {
            Festival _festival = fr.save(new Festival(a.getName(), a.getCity(), a.getCountry()));
            return new ResponseEntity<>(_festival, HttpStatus.CREATED);
        }
        catch(Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Festival> updateFestival(@PathVariable("id") long id, @RequestBody Festival a)
    {
        Optional<Festival> festivalData = fr.findById(id);

        if(festivalData.isPresent())
        {
            Festival _festival = festivalData.get();
            _festival.setName(a.getName());
            _festival.setCity(a.getCity());
            _festival.setCountry(a.getCountry());
            return new ResponseEntity<>(fr.save(_festival), HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteFestival(@PathVariable("id") long id)
    {
        try
        {
            fr.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
