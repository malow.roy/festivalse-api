package com.example.festival_utils.repository;

import com.example.festival_utils.entity.admin.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<Admin, Long> {
    Optional<Admin> findByEmail(String email);
}
