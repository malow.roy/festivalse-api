package com.example.festival_utils.repository;

import com.example.festival_utils.model.Festival;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FestivalRepository  extends JpaRepository<Festival, Long> {

}
